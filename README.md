# ArcGIS Analysis Tool For Projecting Climate Impacts in Pacific Marine Protected Areas

__Main author:__  Sarah Friesen  
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Pacific Biological Station   
__Contact:__      e-mail: Sarah.Friesen@dfo-mpo.gc.ca | tel: 778-678-5022


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Acknowledgements](#acknowledgements)
- [References](#references)


## Objective
The objective of this ArcGIS geoprocessing tool is to assess physical impacts within Pacific Region marine protected areas due to projected future seasonal environmental conditions under the “no-mitigation scenario” Representative Concentration Pathway (RCP) 8.5.


## Summary
This project assesses seasonal sea surface and benthic temperature, aragonite saturation state, and dissolved oxygen using outputs from two regional ocean models: the British Columbia Continental Margin Model (BCCM) and the Northeastern Pacific Canadian Ocean Ecosystem Model (NEP36-CanOE). It calculates the mean, minimum, maximum, and range of model output values (and change in model output values) intersecting MPAs in the Northern Shelf, Southern Shelf, and Offshore Pacific Bioregions. This analysis tool may be run using polygons of marine protected areas or any other boundaries of interest, but all other input layers need to come from the data package provided.

The source data packages and result packages for each bioregion may be accessed on the MSEA GIS Hub (https://www.gis-hub.ca/group/climate-change-mpas). Downloading the source data packages first requires permission from Angelica Peña for the BCCM layers (Angelica.Pena@dfo-mpo.gc.ca) and Amber Holdsworth for the NEP36-CanOE layers (Amber.Holdsworth@dfo-mpo.gc.ca). 

The technical report that should be cited for this tool is:  
Friesen, S.K., Ban, N.C., Holdsworth, A.M., Peña, M.A., Christian, J. and Hunter, K.L. 2021. Physical impacts of projected climate change within marine protected areas in the Pacific Bioregions. Can. Tech. Rep. Fish. Aquat. Sci. 3422: iv + 60 p.


## Status
Completed


## Contents
`User manual for MPA spatial analysis tool.docx` - user manual for the geoprocessing tool with step by step instructions for running the tool and connecting it to the underlying source script

`MPA_Analysis_Tool.tbx` - ArcGIS toolbox containing the geoprocessing tool for the analysis: “MPA_Analysis_Tool”

`script_for_MPA_analysis_tool.py` - underlying source script for the geoprocessing tool; must be set as the source script for the tool before running the analysis (see user manual)


## Methods
__Text below taken from Can. Tech. Rep. Fish. Aquat. Sci. 3422. This data package includes the geoprocessing tool described in the last paragraph of the methods__

### Description of regional ocean models
Two regional ocean models have been developed for the British Columbia continental margin and we used these to analyze three Pacific bioregions. Each model has outputs for a historical or hindcast time period, and a projected future time period under RCP8.5. The models have been developed using different frameworks and parameterizations, so using both was helpful for understanding potential changes that may occur in the bioregions. Moreover, the ocean models differ in their spatial coverage, in the forcing imposed at the surface and lateral boundaries, and cover different time periods. 

The Northeastern Pacific Canadian Ocean Ecosystem Model (NEP36-CanOE) was developed by Amber Holdsworth, James Christian, and others at Fisheries and Oceans Canada (DFO). This model is a regional configuration of Nucleus for European Modelling of the Ocean (NEMO 3.6; Madec 2008). The NEP36-CanOE model domain spans the Canadian Pacific Ocean east of 140°W and north of 45°N with a spatial resolution of 1/36° (1.5-2.25 km) and 50 vertical levels. Model simulations were downscaled from Historical and Representative Concentration Pathway (4.5, 8.5) experiments with the second generation Canadian Earth System Model (CanESM2) suite of global climate models. A more complete description of the model, forcing fields, and validation with observations can be found in Holdsworth et al. (2021). Because the model was forced with atmospheric climatologies (with augmented winds), model outputs represent climatologies of the historical 1986-2005 period and future 2046-2065 projections. In deep ocean areas (e.g., > 500 m), the NEP36-CanOE model has not been run long enough to reach equilibrium, so model outputs for these areas may not be very different from the CanESM2 outputs from which the initial conditions were obtained.

The ocean circulation-biogeochemical model for the British Columbia continental margin (BCCM) was developed by Angelica Peña and others at DFO. The model is a Regional Ocean Modeling System (ROMS; Haidvogel et al. 2008) implementation and covers the Canadian west coast, extending from the Alaska border (~51 °N) to south of the Columbia River (~47 °N), and out to about 400 km from the shore. The model grid has a horizontal resolution of 3 km and a vertical resolution of 42 non-uniform sigma levels. A detailed description of the BCCM model, forcing fields, and validation with observations is given in Peña et al. (2019). The model outputs used in this study are from a 30-year long hindcast simulation for the period 1981-2010 (Peña et al. 2019) and a future projection for the period 2041-2070. Climate projections from the Canadian regional and global climate model (CanRCM4/CanESM2) were used to create a climate change perturbation (differences between projection under RCP8.5 from 2041 to 2070 and historical simulation from 1981 to 2010) that was added to the BCCM hindcast simulation to carry out the future regional projection (Peña et al. 2018).

### Analysis methods
We generated the model output extents used for our analysis by first excluding areas with high data uncertainty using ArcGIS 10.7.1 (ESRI Inc. 2019). The British Columbia coastline is extremely convoluted with many narrow channels and inlets that are not adequately resolved by the models. Therefore, we excluded these nearshore areas (Table 1). We then converted the model output extents to rasters with square 3 x 3 km grid cells using inverse distance squared weighted interpolation to calculate the new grid cell values (IDW tool). This interpolation was done from the centroid of the raster grid cell being calculated and considered any data points within one grid cell’s diagonal distance (4243 m). There was no overlap between the model output extents and the Strait of Georgia Bioregion. Models were validated at the discrete locations where observations were available including, for example, lighthouse data and shipboard observations, but these observational data do not cover the entirety of the Pacific Region; see Masson & Fine (2012), Peña et al. (2019), and Holdsworth et al. (2021) for details of validation.

To calculate projected seasonal changes in environmental parameters for each raster grid cell, we subtracted the model output values for the hindcast or historical time period from model output values for the projected future time period. Seasons were split into winter (Dec-Jan-Feb), spring (Mar-Apr-May), summer (Jun-Jul-Aug), and fall (Sep-Oct-Nov) as in Morrison et al. (2014). We determined projected a) benthic changes and b) sea surface changes in: 1) temperature and 2) aragonite saturation state across the entire Northern Shelf Bioregion. We also evaluated projected benthic changes in 3) dissolved oxygen for the bioregion using NEP36-CanOE model outputs only. 

For this analysis, we considered the existing federal and provincial MPAs in each bioregion, plus the Offshore Pacific Area of Interest (AOI) being considered for designation as an Oceans Act MPA. Specifically, we identified the marine components of existing federal MPAs (i.e., Oceans Act MPAs, National Marine Conservation Area Reserves, National Park Reserves, Marine National Wildlife Areas, and Migratory Bird Sanctuaries) and provincial MPAs (i.e., Class A Parks, Conservancies, Ecological Reserves, Protected Areas, and Wildlife Management Areas) in the Canadian Protected and Conserved Areas Database (Environment and Climate Change Canada 2020). We did not consider other effective area-based conservation measure (OECM) polygons for this analysis. The MPAs and Offshore Pacific AOI totaled 152 MPA polygons across the three bioregions including the Offshore Pacific AOI, but we subdivided three MPA polygons. First, there are three spatially distinct sites (Northern Reef, Central Reefs, and Southern Reef) within the Hecate Strait and Queen Charlotte Sound Glass Sponge Reefs MPA, so each site was considered a separate MPA polygon for this analysis. Second, the Scott Islands Marine National Wildlife Area overlaps both the Northern Shelf Bioregion and Offshore Pacific Bioregion; both portions of this MPA are large so they were considered as separate MPA polygons to make the summary statistics more meaningful. Third, the Offshore Pacific AOI is an expansive contiguous polygon; it was divided into three very simplified oceanographic zones (coastal upwelling zone, upwelling/downwelling transition zone, and bifurcation zone) as in Du Preez and Norgard (in review); see details of subdivision in Appendix C. We used these zones to compute summary statistics on smaller polygons within the AOI, but it should be noted that the boundaries between these oceanographic zones are highly temporally- and spatially-variable. With these subdivisions, 157 MPA polygons were identified across the three Pacific bioregions.

We developed an ArcGIS geoprocessing tool to compute summary spatial statistics for the environmental parameter values in each included MPA polygon. The first step in the tool’s workflow was to include only those MPA polygons with at least 50% area overlap with the model output extents in the analysis (Table 2). Because of this, 23 MPA polygons were included in the BCCM analysis and 16 MPAs were included in the NEP36-CanOE analysis. The environmental parameter rasters were adjusted to 300 m resolution, aligned such that 100 new grid cells of identical value were created within each original grid cell of 3000 m resolution. This change in resolution was done to more closely align the raster resolution with the size of smaller MPAs so that zonal statistics could be properly computed, but did not change the value of any grid cells (ESRI Inc. n.d.). The included MPA polygons were also converted to a raster with 300 m resolution, aligned with the environmental parameter rasters; where multiple MPAs were present within the same raster grid cell, priority was given to the MPA with the smallest total area to ensure that these MPAs were represented within the raster. The tool determined which parameter raster grid cells intersected each rasterized MPA then computed zonal statistics to calculate the mean, minimum, maximum, and range of the intersecting grid cells for each MPA. Any MPA area outside the model output extent was not considered in the calculation. These MPA summary statistics were generated for all environmental parameters in the historical or hindcast time period and projected future time period for both regional ocean models, as well as the change in parameter value between time periods. This technical report presents results for the mean environmental parameter value within MPAs only; result layers for mean, minimum, maximum, and range calculations may be accessed on the DFO Marine Spatial Ecology & Analysis Section’s GIS Hub (https://www.gis-hub.ca/group/climate-change-mpas).


## Requirements
This tool has been designed to run in ArcGIS, with inputs entered through a user interface in ArcCatalog or ArcMap. The underlying Python script uses the `arcpy` module which requires an ESRI ArcGIS and Spatial Analyst license. 


## Caveats
The tool cannot be run without the source data packages for each bioregion that may be accessed on the MSEA GIS Hub (https://www.gis-hub.ca/group/climate-change-mpas). Permission must be obtained from Angelica Peña for the BCCM layers (Angelica.Pena@dfo-mpo.gc.ca) and Amber Holdsworth for the NEP36-CanOE layers (Amber.Holdsworth@dfo-mpo.gc.ca). The tool may be run using polygons of marine protected areas or any other boundaries of interest, but all other input layers need to come from the data package provided.


## Acknowledgements
Natalie C. Ban, Angelica Peña, Amber Holdsworth, James Christian, & Karen Hunter


## References
__Friesen, S.K., Ban, N.C., Holdsworth, A.M., Peña, M.A., Christian, J. and Hunter, K.L. 2021. Physical impacts of projected climate change within marine protected areas in the Pacific Bioregions. Can. Tech. Rep. Fish. Aquat. Sci. 3422: iv + 60 p. [https://waves-vagues.dfo-mpo.gc.ca/Library/40974650.pdf](https://waves-vagues.dfo-mpo.gc.ca/Library/40974650.pdf)__

Holdsworth, A.M., Zhai, L., Lu, Y., and Christian, J.R. 2021. Future changes in oceanography and biogeochemistry along the Canadian Pacific continental margin. Frontiers in Marine Science 8: 602991.

Masson, D., and Fine, I. 2012. Modeling seasonal to interannual ocean variability of coastal British Columbia. Journal of Geophysical Research: Oceans 117: C10019. doi:10.1029/2012JC008151.

Peña, A., Fine, I., and Callendar, W. 2019. Interannual variability in primary production and shelf-offshore transport of nutrients along the northeast Pacific Ocean margin. Deep-Sea Research Part II 169: 104637.
