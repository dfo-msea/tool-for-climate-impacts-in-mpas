# -*- coding: utf-8 -*-
"""
Tool for determining parameter values within marine protected areas in the Northern Shelf Bioregion
@author: Sarah Friesen
"""

import arcpy

# Check out ArcGIS ArcGIS Spatial Analyst extension license
arcpy.CheckOutExtension("Spatial")

##############################################################################
# Obtain Model Inputs From User
##############################################################################

# Shapefile of bioregion for determining MPA area overlap for MPAs that are in multiple bioregions (like the Scott Islands MNWA)
Bioregion = arcpy.GetParameterAsText(0)

# Feature class containing draft MPA network polygons
Draft_MPA_Network = arcpy.GetParameterAsText(1)

# Get unique MPA ID column within feature class containing draft MPA network polygons
MPA_ID = arcpy.GetParameterAsText(2)

# Feature class with extent of the BCCM model data - used to clip the draft MPA network for processing
BCCM_OceanExtent = arcpy.GetParameterAsText(3)

# Feature class with extent of the NEP36 model data - used to clip the draft MPA network for processing
NEP36_OceanExtent = arcpy.GetParameterAsText(4)

# User specified file geodatabase containing input rasters from BCCM model - extents should match the BCCM ocean extent
Input_GDB_for_BCCM_Values = arcpy.GetParameterAsText(5)

# User specified file geodatabase containing input rasters from NEP36 model - extents should match the NEP36 ocean extent
Input_GDB_for_NEP36_Values = arcpy.GetParameterAsText(6)

# User specified geodatabase location for final result output
ResultLocation = arcpy.GetParameterAsText(7)

# User specified geodatabase location for intermediate files created during analysis
ScratchGDB = arcpy.GetParameterAsText(8)


##############################################################################
# Prep copy of MPA network for analysis
##############################################################################

arcpy.AddMessage("Prepping MPAs")

# Use scratchGDB environment to write intermediate data, and copy the MPAs to get a working temp copy and preserve original input
out_filename = ScratchGDB + "\MPANetworkCopy"
MPANetworkCopy = arcpy.Copy_management(Draft_MPA_Network, out_filename)

# Clip all MPAs by the bioregion of interest so that the proportion of area overlap with the model extent is calculated against the MPA area in the bioregion
out_filename = ScratchGDB + "\AllMPAs_InBioregion"
MPAsInBioregion = arcpy.Clip_analysis(MPANetworkCopy, Bioregion, out_filename)

# Add separate field and calculate total area of MPAs in square meters
arcpy.AddField_management(MPAsInBioregion, "MPA_TotalArea", "DOUBLE", field_is_nullable="NULLABLE")
arcpy.CalculateField_management(MPAsInBioregion, "MPA_TotalArea", '!Shape!.getArea("PLANAR","SQUAREMETERS")', 'PYTHON')


##############################################################################
# Process the MPAs using the BCCM model
##############################################################################

arcpy.AddMessage("Clipping MPAs by BCCM extent")

# Clip the temp MPA layer
out_filename = ScratchGDB + "\AllMPAs_ClippedByBCCM"
ClippedMPAsBCCM = arcpy.Clip_analysis(MPAsInBioregion, BCCM_OceanExtent, out_filename)

# Calculate area of clipped MPAs
arcpy.AddField_management(ClippedMPAsBCCM, "MPA_ClippedArea", "DOUBLE", field_is_nullable="NULLABLE")
arcpy.CalculateField_management(ClippedMPAsBCCM, "MPA_ClippedArea", '!Shape!.getArea("PLANAR","SQUAREMETERS")', 'PYTHON')

# Calculate proportion of overlap of original and clipped MPA areas
arcpy.AddField_management(ClippedMPAsBCCM, "PropMPAOverlap", "DOUBLE", field_is_nullable="NULLABLE")
arcpy.CalculateField_management(ClippedMPAsBCCM, "PropMPAOverlap", '!MPA_ClippedArea! / !MPA_TotalArea!', 'PYTHON')

# Select the subset of MPAs that have greater than or equal to 50% overlap with the BCCM model
out_filename_mean = ScratchGDB + "\MPAs_mean"
out_filename_min = ScratchGDB + "\MPAs_min"
out_filename_max = ScratchGDB + "\MPAs_max"
out_filename_range = ScratchGDB + "\MPAs_range"

BCCM_mean = arcpy.Select_analysis(ClippedMPAsBCCM, out_filename_mean, '"PropMPAOverlap" >= 0.5')
BCCM_min = arcpy.Select_analysis(ClippedMPAsBCCM, out_filename_min, '"PropMPAOverlap" >= 0.5')
BCCM_max = arcpy.Select_analysis(ClippedMPAsBCCM, out_filename_max, '"PropMPAOverlap" >= 0.5')
BCCM_range = arcpy.Select_analysis(ClippedMPAsBCCM, out_filename_range, '"PropMPAOverlap" >= 0.5')


# Abort analysis if feature class is empty because there are no MPAs with >=50% overlap with the BCCM model extent  
nrows = str(arcpy.GetCount_management(BCCM_mean))
arcpy.AddMessage("Count equals " + str(nrows) + " MPAs within BCCM ocean extent")  
if nrows == "0":
    arcpy.AddMessage("Skipping BCCM analysis")
else:
    
    ##############################################################################
    # Hardcoded Variables
    ##############################################################################
    
    # The resolution to resample BCCM data to (allows zonal statistics to more accurately integrate the area covered by each BCCM grid cell into its calculation by resampling to 100 pixels per grid cell)
    BCCM_Resample_Res = 300
    
    # Naming the result file paths for BCCM values
    BCCM_mean_path = ResultLocation+"\BCCM_AveragedByMPA_Mean"
    BCCM_min_path = ResultLocation+"\BCCM_AveragedByMPA_Min"
    BCCM_max_path = ResultLocation+"\BCCM_AveragedByMPA_Max"
    BCCM_range_path = ResultLocation+"\BCCM_AveragedByMPA_Range"
    
    ##############################################################################
    # Convert MPAs to raster matching the resampled resolution of the BCCM data
    ##############################################################################
    
    # Get a list of all the rasters in the BCCM input FGDB to iterate
    arcpy.env.workspace = Input_GDB_for_BCCM_Values
    BCCMRasters = arcpy.ListRasters("*", "All")
    arcpy.env.snapRaster = BCCMRasters[0]
    
    # Resample the first BCCM raster in the list to 300m square resolution
    out_filename = ScratchGDB + "\BCCM_resampled"
    BCCMRasterReSampled = arcpy.Resample_management(BCCMRasters[0], out_filename, BCCM_Resample_Res, "NEAREST")
    
    # Create field in MPA attribute table so that smallest MPAs are given priority
    arcpy.AddField_management(BCCM_mean, "InverseArea", "DOUBLE", field_is_nullable="NULLABLE")
    arcpy.CalculateField_management(BCCM_mean, "InverseArea", '1000000 / !Shape_Area!', 'PYTHON')
    
    # Rasterize MPAs
    out_filename = ScratchGDB + "\MPAs_BCCM_rst"
    ClippedMPAsFilteredRasterBCCM = arcpy.PolygonToRaster_conversion(BCCM_mean, MPA_ID, out_filename, "MAXIMUM_AREA", 'InverseArea', BCCMRasterReSampled)
    
    
    ##############################################################################
    # Do Batch Zonal Stats as Table for BCCM data using filtered MPA dataset
    ##############################################################################
    arcpy.env.overwriteOutput = True
    
    count = 1
    for raster in BCCMRasters:
        out_raster = ScratchGDB + "\\vrst"
        BCCMValueRasterResampled = arcpy.Resample_management(raster, out_raster, BCCM_Resample_Res, "NEAREST")
        
        out_filename = ScratchGDB + "\zonalstattable"
        BCCMZonalStatsTable = arcpy.sa.ZonalStatisticsAsTable(ClippedMPAsFilteredRasterBCCM, 'Value', BCCMValueRasterResampled, out_filename, "DATA", "ALL")
                
        arcpy.JoinField_management(BCCM_mean, MPA_ID, BCCMZonalStatsTable, 'Value', ['MEAN'])
        arcpy.AlterField_management(BCCM_mean, 'MEAN', "mean_"+raster)
        
        arcpy.JoinField_management(BCCM_min, MPA_ID, BCCMZonalStatsTable, 'Value', ['MIN'])
        arcpy.AlterField_management(BCCM_min, 'MIN', "min_"+raster)
        
        arcpy.JoinField_management(BCCM_max, MPA_ID, BCCMZonalStatsTable, 'Value', ['MAX'])
        arcpy.AlterField_management(BCCM_max, 'MAX', "max_"+raster)
        
        arcpy.JoinField_management(BCCM_range, MPA_ID, BCCMZonalStatsTable, 'Value', ['RANGE'])
        arcpy.AlterField_management(BCCM_range, 'RANGE', "rng_"+raster)
        
        arcpy.AddMessage("Finished processing " + str(count) + " of " + str(len(BCCMRasters)) + " rasters for BCCM")
        count = count + 1
    
    # Delete column with inverse area values
    arcpy.DeleteField_management(BCCM_mean, ["InverseArea"])
    arcpy.DeleteField_management(BCCM_min, ["InverseArea"])
    arcpy.DeleteField_management(BCCM_max, ["InverseArea"])
    arcpy.DeleteField_management(BCCM_range, ["InverseArea"])
    
    # Export result files
    arcpy.Copy_management(BCCM_mean, BCCM_mean_path)
    arcpy.Copy_management(BCCM_min, BCCM_min_path)
    arcpy.Copy_management(BCCM_max, BCCM_max_path)
    arcpy.Copy_management(BCCM_range, BCCM_range_path)
##############################################################################


##############################################################################
# Process the MPAs using the NEP36 model
##############################################################################

arcpy.AddMessage("Clipping MPAs by NEP36-CanOE extent")

# Clip the temp MPA layer 
out_filename = ScratchGDB + "\AllMPAs_ClippedByNEP36"
ClippedMPAsNEP36 = arcpy.Clip_analysis(MPAsInBioregion, NEP36_OceanExtent, out_filename)

# Calculate area of clipped MPAs
arcpy.AddField_management(ClippedMPAsNEP36, "MPA_ClippedArea", "DOUBLE", field_is_nullable="NULLABLE")
arcpy.CalculateField_management(ClippedMPAsNEP36, "MPA_ClippedArea", '!Shape!.getArea("PLANAR","SQUAREMETERS")', 'PYTHON')

# Calculate proportion of overlap of original and clipped MPA areas
arcpy.AddField_management(ClippedMPAsNEP36, "PropMPAOverlap", "DOUBLE", field_is_nullable="NULLABLE")
arcpy.CalculateField_management(ClippedMPAsNEP36, "PropMPAOverlap", '!MPA_ClippedArea! / !MPA_TotalArea!', 'PYTHON')

# Select the subset of MPAs that have greater than or equal to 50% overlap with the NEP36 model
out_filename_mean = ScratchGDB + "\MPAs_mean"
out_filename_min = ScratchGDB + "\MPAs_min"
out_filename_max = ScratchGDB + "\MPAs_max"
out_filename_range = ScratchGDB + "\MPAs_range"

NEP36_mean = arcpy.Select_analysis(ClippedMPAsNEP36, out_filename_mean, '"PropMPAOverlap" >= 0.5')
NEP36_min = arcpy.Select_analysis(ClippedMPAsNEP36, out_filename_min, '"PropMPAOverlap" >= 0.5')
NEP36_max = arcpy.Select_analysis(ClippedMPAsNEP36, out_filename_max, '"PropMPAOverlap" >= 0.5')
NEP36_range = arcpy.Select_analysis(ClippedMPAsNEP36, out_filename_range, '"PropMPAOverlap" >= 0.5')


# Abort analysis if feature class is empty because there are no MPAs with >=50% overlap with the BCCM model extent  
nrows = str(arcpy.GetCount_management(NEP36_mean))
arcpy.AddMessage("Count equals " + str(nrows) + " MPAs within NEP36 ocean extent")  
if nrows == "0":
    arcpy.AddMessage("Skipping NEP36 analysis")
else:  
    
    ##############################################################################
    # Hardcoded Variables
    ##############################################################################
    
    # The resolution to resample NEP36 data to (allows zonal statistics to more accurately integrate the area covered by each NEP36 grid cell into its calculation by resampling to 100 pixels per grid cell)
    NEP36_Resample_Res = 300
    
    # Naming the result file paths for NEP36 values
    NEP36_mean_path = ResultLocation+"\NEP36_AveragedByMPA_Mean"
    NEP36_min_path = ResultLocation+"\NEP36_AveragedByMPA_Min"
    NEP36_max_path = ResultLocation+"\NEP36_AveragedByMPA_Max"
    NEP36_range_path = ResultLocation+"\NEP36_AveragedByMPA_Range"
    
    ##############################################################################
    # Convert MPAs to raster matching the resampled resolution of the NEP36 data
    ##############################################################################
    
    # Get a list of all the rasters in the NEP36 input FGDB to iterate
    arcpy.env.workspace = Input_GDB_for_NEP36_Values
    NEP36Rasters = arcpy.ListRasters("*", "All")
    arcpy.env.snapRaster = NEP36Rasters[0]
    
    # Resample the first NEP36 raster in the list to 200m square resolution
    out_filename = ScratchGDB + "\NEP36_resampled"
    NEP36RasterReSampled = arcpy.Resample_management(NEP36Rasters[0], out_filename, NEP36_Resample_Res, "NEAREST")
    
    # Create field in MPA attribute table so that smallest MPAs are given priority
    arcpy.AddField_management(NEP36_mean, "InverseArea", "DOUBLE", field_is_nullable="NULLABLE")
    arcpy.CalculateField_management(NEP36_mean, "InverseArea", '1000000 / !Shape_Area!', 'PYTHON')  
    
    # Rasterize MPAs
    out_filename = ScratchGDB + "\MPAs_NEP_rst"
    ClippedMPAsFilteredRasterNEP36 = arcpy.PolygonToRaster_conversion(NEP36_mean, MPA_ID, out_filename, "MAXIMUM_AREA", 'InverseArea', NEP36RasterReSampled)
    
    
    ##############################################################################
    # Do Batch Zonal Stats as Table for NEP36 data using filtered MPA dataset
    ##############################################################################
    arcpy.env.overwriteOutput = True
    
    count = 1
    for raster in NEP36Rasters:
        out_raster = ScratchGDB + "\\vrst"
        NEP36ValueRasterResampled = arcpy.Resample_management(raster, out_raster, NEP36_Resample_Res, "NEAREST")
        
        out_filename = ScratchGDB + "\zonalstattable"
        NEP36ZonalStatsTable = arcpy.sa.ZonalStatisticsAsTable(ClippedMPAsFilteredRasterNEP36, 'Value', NEP36ValueRasterResampled, out_filename, "DATA", "ALL")
                        
        arcpy.JoinField_management(NEP36_mean, MPA_ID, NEP36ZonalStatsTable, 'Value', ['MEAN'])
        arcpy.AlterField_management(NEP36_mean, 'MEAN', "mean_"+raster)
        
        arcpy.JoinField_management(NEP36_min, MPA_ID, NEP36ZonalStatsTable, 'Value', ['MIN'])
        arcpy.AlterField_management(NEP36_min, 'MIN', "min_"+raster)
        
        arcpy.JoinField_management(NEP36_max, MPA_ID, NEP36ZonalStatsTable, 'Value', ['MAX'])
        arcpy.AlterField_management(NEP36_max, 'MAX', "max_"+raster)
        
        arcpy.JoinField_management(NEP36_range, MPA_ID, NEP36ZonalStatsTable, 'Value', ['RANGE'])
        arcpy.AlterField_management(NEP36_range, 'RANGE', "rng_"+raster)
                
        arcpy.AddMessage("Finished processing " + str(count) + " of " + str(len(NEP36Rasters)) + " rasters for NEP36-CanOE")
        count = count + 1
    
    # Delete column with inverse area values
    arcpy.DeleteField_management(NEP36_mean, ["InverseArea"])
    arcpy.DeleteField_management(NEP36_min, ["InverseArea"])
    arcpy.DeleteField_management(NEP36_max, ["InverseArea"])
    arcpy.DeleteField_management(NEP36_range, ["InverseArea"])
   
    # Export result files
    arcpy.Copy_management(NEP36_mean, NEP36_mean_path)
    arcpy.Copy_management(NEP36_min, NEP36_min_path)
    arcpy.Copy_management(NEP36_max, NEP36_max_path)
    arcpy.Copy_management(NEP36_range, NEP36_range_path)
